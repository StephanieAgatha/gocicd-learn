FROM golang:1.21.3 as builder

#set workdir
WORKDIR /app

#copy all to /app
COPY . /app/

#go build
RUN GOOS=linux CG_ENABLED=0 go build -o gocicd

#declare step final
FROM golang:latest

# Menyalin hasil build dari step sebelumnya
# ke directory /usr/local/bin
COPY --from=builder ./app/ /usr/local/bin

CMD ["./gocicd"]